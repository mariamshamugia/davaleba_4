package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class Answers : Fragment(R.layout.fragment_answers) {

    lateinit var finalAnswer1: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        finalAnswer1 = view.findViewById<TextView>(R.id.finalAnswer1)
        finalAnswer1.text = AnswersArgs.fromBundle(requireArguments()).number.toString()



    }

}