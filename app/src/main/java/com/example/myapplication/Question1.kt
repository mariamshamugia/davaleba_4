package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.Navigation

class Question1 : Fragment(R.layout.fragment_question1) {

    lateinit var question1: TextView
    lateinit var answer1: TextView
    lateinit var buttonNext1: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        question1 = view.findViewById(R.id.question1)
        answer1 = view.findViewById(R.id.question2)
        buttonNext1 = view.findViewById(R.id.buttonNext1)

        val navController = Navigation.findNavController(view)

        buttonNext1.setOnClickListener{
            val answer = answer1.text.toString()

            if(answer.isEmpty()){
                return@setOnClickListener
            }

            val answer2 = answer.toInt()

            val action = Question1Directions.actionQuestion1ToAnswers(answer2)

            navController.navigate(action)




        }


    }

}